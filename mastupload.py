#!/usr/bin/env python3
# -*- encoding: utf-8; py-indent-offset: 4 -*-
"""Upload photos with EXIF metadata to Mastodon"""

from pathlib import Path
import errno
import configparser
import logging
import argparse
import imghdr
import exiftool
from mastodon import Mastodon

try:
    import ssl  # Imports TLS library; Legacy SSL is not supported anymore by
    # OpenSSL and other crypto libs.
    # print("TLS supported on this client\n")
except ImportError:
    print("Error: no TLS support on this client\nAre you sure you\
 really want to transmit your password in plaintext?\n\
 Remove this exception at your own risk\n")
    exit(2)


def load_config():
    """Loads config file"""
#   #Initalize variables, from config file
    conffile = Path(__file__).parent/"conf/fedi.cfg"
    config = configparser.ConfigParser()
    try:
        with open(conffile) as filedesc:
            config.read_file(filedesc)
    except OSError as ioerr:
        if ioerr.errno == errno.ENOENT:
            print(f"Config file '{conffile}' not found.")
            exit(4)
        else:
            print(f"I/O Error :\n{ioerr}")
            exit(5)
    except (configparser.MissingSectionHeaderError,
            configparser.NoSectionError,
            configparser.NoOptionError) as conferror:
        print(f"{conferror}")
        exit(6)

    return config


def initlogging(debug):
    """Initialize logger"""
    if debug is False:
        loglevel = logging.INFO
    else:
        loglevel = logging.DEBUG

    logger = logging.getLogger()
    logger.setLevel(loglevel)

    # create console handler
    handler = logging.StreamHandler()
    handler.setLevel(loglevel)
    formatter = logging.Formatter("%(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger


def parseargs(feditags, logger):
    """Parse script's arguments"""
    logger.debug("Inside 'parseargs' function")
    parser = argparse.ArgumentParser(description='Process options for\
 JPEG data (in JFIF or EXIF format) to upload.')
    parser.add_argument('-f', '--file', dest='filename', action='store')
    parser.add_argument('-t', '--tags', dest='usertags', action='store')
    parser.add_argument('-d', '--description',
                        dest='description', action='store')
    args = parser.parse_args()
    logger.debug("CLI arguments:\n%s\n", args)

    if args.filename is not None:
        filename = args.filename
        try:
            # Checking if filetype is actually JPEG data in JFIF or EXIF
            # format, but assumes file is valid
            # (e.g. not truncated or otherwise broken… )
            if imghdr.what(filename) != "jpeg":
                logger.error("%s\nProvided file doesn't contain JPEG data",
                             filename)
                exit(3)
        except OSError as ioerr:
            logger.error(ioerr)
            exit(2)
    else:
        logger.error("Input file's path ('-f' arg) is required\n\
USAGE: ./%s -f|--file /home/username/path/to/photo.jpg", parser.prog)
        exit(1)

    if args.description is not None:
        description = f"{args.description}\n".replace("\\n", "\n")
    else:
        description = input("Please, provide a description\
 if you wish (optional, press enter to proceed).\n").replace("\\n", "\n")
        if description:
            description = description + "\n"

    if args.usertags is not None:
        usertags = args.usertags.split()
        if feditags is not None:
            usertags = ' '.join(usertags) + ' '
            feditags = usertags + feditags
        else:
            usertags = ' '.join(usertags)
            feditags = usertags

    if feditags is not None:
        feditags = feditags.split()
        feditags = ['#' + hashtag for hashtag in feditags]
        feditags = ' '.join(feditags)
        post = f"{description}{feditags}"
    else:
        post = f"{description}"

    logger.debug("Text content for post:\n%s\n", post)
    return filename, post


def exif_conversion(crop_factor, ff_efl, exposure_program, logger):
    """Convert and format EXIF"""
    logger.debug("Inside 'exif_conversion' function")
    logger.debug("Crop factor: %s\n", crop_factor)
    if crop_factor != 1.0:
        ff_efl = f" (FF equiv. {ff_efl} mm)"
    else:
        # 35mm equiv. focale lenght variable should be empty
        # if sensor's crop_factor == 1 (Fullframe)
        ff_efl = f""

    sensors = {}
    sensors["0.6"] = 'Medium format'
    sensors["0.7"] = 'Medium format'
    sensors["0.8"] = 'Medium format'
    sensors["1.0"] = 'Fullframe'
    sensors["1.3"] = 'APS-H'
    sensors["1.5"] = 'APS-C'
    sensors["1.6"] = 'APS-C'
    sensors["1.7"] = 'Old Foveon X3 sensor (pre-Merill)'
    sensors["1.8"] = 'Micro 4/3'
    sensors["1.9"] = 'Micro 4/3'
    sensors["2.0"] = 'Micro 4/3'
    sensors["2.7"] = '1" sensor'
    sensors["3.9"] = '2/3" sensor'
    sensors["4.5"] = '1/1.7" sensor'
    sensors["4.7"] = '1/1.7" sensor'  # Canon Powershot G11
    sensors["4.8"] = '1/1.8" sensor'
    sensors["5.6"] = '1/2.3" sensor'
    sensors["6.0"] = '1/2.5" sensor'

    crop_factor = sensors.get(f"{crop_factor:.1f}")

    expoprg = {}
    expoprg["0"] = "Not defined"
    expoprg["1"] = "Manual"
    expoprg["2"] = "Normal program"
    expoprg["3"] = "Aperture priority"
    expoprg["4"] = "Shutter priority"
    expoprg["5"] = "Creative program"
    expoprg["6"] = "Action program"
    expoprg["7"] = "Portrait mode"
    expoprg["8"] = "Landscape mode"

    exposure_program = expoprg.get(f"{exposure_program}")
    if exposure_program is None:
        exposure_program = input("ExposureProgram not found, \
please provide it manually\n")

    return (crop_factor, ff_efl, exposure_program)


def exif_extraction(filename, config, logger):
    """Extact EXIF data for each photo, and add Mastodon custom emojis"""
    with exiftool.ExifTool() as exift:
        exposure_program = exift.get_tag("EXIF:ExposureProgram", filename)
        ff_efl = exift.get_tag("Composite:FocalLength35efl", filename)
        ff_efl = f"{ff_efl:.1f}"
        crop_factor = exift.get_tag("Composite:ScaleFactor35efl", filename)
        crop_factor = round(crop_factor, 1)  # Exact value is unusable
        focal_length = exift.get_tag("EXIF:FocalLength", filename)
        fnumber = exift.get_tag("EXIF:FNumber", filename)
        exposure_time = exift.get_tag("EXIF:ExposureTime", filename)
        iso = exift.get_tag("EXIF:ISO", filename)

    crop_factor, ff_efl, exposure_program = exif_conversion(crop_factor,
                                                            ff_efl,
                                                            exposure_program,
                                                            logger)

    if float(exposure_time) < 1:
        exposure_time = "1/" + f"{1/exposure_time:.0f}"

    useicons = config.getboolean("RUNOPT", "useicons")
    if useicons is True:
        exifmetadata = f"{config.get('ICONS', 'camera')} \
{crop_factor}, {exposure_program}\n\
{config.get('ICONS', 'lens')} {focal_length} mm{ff_efl}\n\
{config.get('ICONS', 'aperture')} F/{fnumber}\n\
{config.get('ICONS', 'shutterspeed')} {exposure_time} s\n\
{config.get('ICONS', 'iso')} {iso}"
    else:
        exifmetadata = f"Exposure: {crop_factor}, {exposure_program}\n\
Focal Length: {focal_length} mm{ff_efl}\n\
F Number: F/{fnumber}\nExposure Time: {exposure_time} s\nISO: {iso}"
    logger.debug("Inside 'exif_extraction' function")
    logger.debug("Extracted EXIF data:\n%s\n", exifmetadata)

    return exifmetadata


def upload(config, filename, post, logger):
    """Upload media to Mastodon"""
    logger.debug("Inside 'upload' function")
    # Disable TLSv1.0 and 1.1
    context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
    context.options |= ssl.OP_NO_TLSv1
    context.options |= ssl.OP_NO_TLSv1_1

    url = config.get("AUTH", "url")
    token = config.get("AUTH", "token")
    logger.debug("URL: %s", url)
    logger.debug("Token file: %s\n", token)
    logger.debug("Filename: %s", filename)
    logger.debug("Post content:\n\n%s", post)

    # Mastodon login info
    logger.debug("URL: %s", url)
    logger.debug("Token file: %s\n", token)

    # Mastodon auth
    mastodon = Mastodon(
        api_base_url=url,
        access_token=token
    )

    photo = mastodon.media_post(filename)
    mastodon.status_post(post, media_ids=photo)

    logger.info("Publishing: Done\n---\nEnd of loop\n---\n\n")


def main():
    """Main function"""
    config = load_config()

    debug = config.getboolean("RUNOPT", "debug")
    publish = config.getboolean("RUNOPT", "publish")
    feditags = config.get("RUNOPT", "feditags")

    logger = initlogging(debug)
    filename, post, = parseargs(feditags, logger)

    logger.debug("Inside 'main' function\n")
    if debug is True:
        iconslist = config._sections["ICONS"]
        dashes = '-' * 32
        logger.debug(dashes)
        logger.debug("{:<13s} {:<18s}".format("EXIF title", "Emoji code"))
        logger.debug(dashes)
        for exiftitle, emojicode in iconslist.items():
            logger.debug("{:<13s} {:<18s}".format(exiftitle, emojicode))
        print("\n")

    exifmetadata = exif_extraction(filename, config, logger)
    post = "%s\n\n%s" % (exifmetadata, post)

    if publish is True:
        logger.info("Text content to post:\n%s\n", post)
        upload(config, filename, post, logger)
    else:
        logger.info("Text content to post:\n\n%s\n---\nEnd of loop\n---\
\n\n", post)


if __name__ == '__main__':
    main()
