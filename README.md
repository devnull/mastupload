# What is mastupload.py

This scripts automates photographs upload to Mastodon instances, with formatted EXIF data and photography-related hashtags (both default ones for all posts, and post-specific ones). The script also offers optional support for

- A description, if you want to add some text/Unicode characters to your post, with the '-d' argument.
- Custom 'hashtags' (anything you want, post-specific), with the '-t' argument.
- Custom emojis for photography related icons, with the 'useicons' boolean option in the config file and if your instance provide these.


# Prerequisites

## Dependencies

### Needed software

- Python3.6 or newer (f-strings support)
- exiftool

### Required python modules

Modules to install:

- Mastodon.py
- PyExifTool

Mandatory module but should be installed by default

- ssl
- some standard/default python3 modules

### Mastodon user account

You also need an account on a Mastodon instance, on a server that supports at least TLSv1.2. This script doesn't support TLSv1.0 and TLSv1.1 on purpose.
This script has been tested on Mastodon v3.1.2, not with older versions. And since it uses Mastodon.py, it should work on future versions as well.

Non-mastodon fediverse servers are not supported, as I use Mastodon. However, if you use any of these and they provide a python3 module, or any library that can used from a python3 script, changing the Mastodon-related code in the *upload* function should be enough to make this script work on other fedi-software.


### Input data
You need of course camera-produced JPEG data (JFIF or EXIF format) with intact EXIF metadata. If you you erased metadata, or if you camera don't have the standard EXIF tags (some tags may be missing on some cameras), you'll get "None" values instead of the expected metadata. While I tried to avoid bugs, and I tested this script on pictures from multiples cameras (my own + pictures netizens sent me, mainly APS-C and Fullframe DSLRs), I can't support all the specific cases, as I have neither enough data-set, nor enough time, or even a way to implement certain things without ended up with a bloated script. See **Known limitations** for more info.


## Configuration

To run, this script required a config file. Some parameters such as authentication info, are user-dependent so you need to fill the provided example file.

```
[RUNOPT]
debug = 0 # Boolean value, 0: off, 1 on.
publish = 1
useicons = 1
feditags = photo photography photographie MastoArt

[AUTH]
url = https://your.mastodon.instance
token = /absolute/path/to/your/text/token/file
; Relative paths are NOT recommended, as they
; are relative to current working directory,
; Not to script's directory

; Add your instance's custom emoji code here
[ICONS]
camera = :apn:
lens = :objo:
aperture = :ouverture:
shutterspeed = :temps_de_pose:
iso = :iso:
```

You can fill 'feditags' with whatever plaintext keywords you wish, these values are automagically converted to "hashtags". Don't put the number sign ("#") in front of keywords. This feature is optional, so you can of course, leave 'feditags' value empty if you don't want to have any default "hashtags".

```
; Empty value for optional feature
feditags = 
```
But **don't remove** this line, or any other one, just remove value for unused options. The only optional config values are:

- feditags
- custom emojis codes (if useicons = 0)

**Note:** You *must* disable 'useicons' if your instance don't provide custom emojis to represent the following photography-related info.

- Camera type: Used for shooting mode and sensor size)
- Lens: Used for the focal length
- Aperture: Used for the F Number
- Shutter speed: Used for… shutter speed
- ISO: Used for… ISO.

If your instance provide these, you need to change the values in '[ICONS]' sections with your instance's custom emojis code.  
Since it's custom emojis, not default Unicode ones, the default config applies only to the instance I use for photo publishing (a private one).


# Usage

```
$ mastupload --help
usage: mastupload.py [-h] [-f FILENAME] [-d DESCRIPTION] [-t USERTAGS]
[…]
$ mastupload -f $HOME/Images/Photography/Moon/fullmoon.jpg -d "Insert some Sonata Arctica song reference here" -t "Moon fullmoon"

```

The '-f' argument is mandatory. others are optional. tags should be provided without the number sign '', it is prefixed automagically.  
Use quotes if you provide multiple tags, as they should be space-separated. Use quote for description, for the same reason. The description fields support newlines (\n). You can also use variables if description or tags are too long, or if you want to fill these variables in a loop to post multiple pictures in a row, or posting from cronjobs.

```
file="$HOME/Images/Photography/Moon/fullmoon.jpg"
desc="Insert some Sonata Arctica song reference here"
ashitloadoftags="Moon fullmoon Lune PleineLune Whatever"
$ mastupload -f ~/Images/Photography/Moon/fullmoon.jpg -d "$desc" -t "$ashitloadoftags"
```

Longs argument are supported as well.

```
$ mastupload --file FILENAME [--description DESCRIPTION] [--tags USERTAGS]
```

<p>
  <img src="images/mastupload_fullmoon.png" alt="Fullmoon photograph posted on Mastodon, from mastupload.py">
</p>

# Exit codes

1: You didn't provide an input file with the '-f' argument  
2: OSError for the input file (e.g. : File not found/wrong path)  
3: The input file does not contain JPEG data in JFIF or EXIF format (or file's header is somehow damaged)  
4: Config file not found  
5: Other OSError on config file  
6: Config file contains errors, such as wrong/missing head sections, sections or options.


# Known limitations

- Current version support only one file (with description and hashtags) per toot. I'm not planning to change that for the moment.
- Some cameras (e.g. Canon Powershot G11) don't provide the standard 'EXIF:ExposureProgram' tag.
   A custom 'MakerNotes:${Makername}ExposureMode' with different a keys/values set is used instead. Such cameras are not supported for Exposure Program detection, because PyExifTool returns int (keys), not values (human readable data), and supporting such cameras would mean
   - Finding out which tag is used for each specific case.
   - writing a shitload of conditions to determine which tag to collect, depending on other tags (that may or not be present).
   - Adding a python dictionary for each specific case.
   My goal is, of course, not to rewrite the whole "translate EXIF to a human-readable format" part of exiftool…
   To address this specific issue, EXIF:ExposureProgram has a None value, the scripts asks for used input, so it adds it to the collected data. it's still way faster that writing everything manually.

- Checks if input file is JPG (image header), but doesn't test if input is corrupt, assumes it is. If you have errors during file opening, check if your input file is actually valid.
- Needs further testing with JPGs from less "mainstream" cameras, as some EXIF tags are not mandatory/fully standardized
  So there might be some unexpected "None" output, or EXIF interpretation "errors" for other tags, for cameras that don't respect standards.
  Although I did my best and I didn't find any missing tag other than 'EXIF:ExposureProgram' for my own, and other people's cameras<sup>1</sup> (Thanks to people who sent me pictures for testing), I don't guarantee that it will work *perfectly* with *all* cameras (It probably won't, too many specific case to fit in a small script, to much time required for implementation and tests), but you can modify the code according your specific use case. If you have a specific question in order to do do the required modifications for your camera, I could help *if, and only if*
  - I have some data to work on: untampered JPEG data from your camera, or from the same camera model as yours.
  - I have some free time to play with exiftool and determine what's wrong.
  But I won't add specific use-case code to the main script, as it would make this script too bloated for what it's supposed to do. Especially for camera-specific/non standard tags, with multiple possible values that needs to be converted to a human readable format (e.g. convert integers to their respective text values)
- I give absolutely 0 support for python on windows/windows-related issues, as I don't use that OS.


1 Including : Canon 1100D/Rebel T3, 760D/Rebel T6s, 5D Mark I, 5D Mark II, and Nikon D750. As well as Canon Powershot G11<sup>2</sup>.  
2 Except for 'EXIF:ExposureProgram' tag.

